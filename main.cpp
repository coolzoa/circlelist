#include <iostream>
#include "CircleList.h"

using namespace std;

int main()
{
    CircleList<int> lista;
    for(int i = 0; i < 21; i++){
        lista.add(i);
    }
    cout << "Se agregan 21 n�meros!" << endl;
    lista.mostrarLista();

    cout << "Esta la lista vacia?" << endl;
    cout << lista.empty() << endl;

    cout << "Elemento actual: " << lista.front() << endl;
    cout << "Se avanza al frente" << endl;
    lista.advance();
    cout << "Elemento actual: " << lista.front() << endl;
    cout << "Elemento anterior: " << lista.back() << endl;

    cout << "Remuevo el elemento actual: " << lista.remove() << endl;

    lista.mostrarLista();

    return 0;
}
